var express = require ('express');
var bodyParser = require ('body-parser');
var path = require ('path');
const mongoose = require('mongoose');

//Import models
let Booking = require('./models/booking');


mongoose.connect('mongodb://mongo/bookme');
let db = mongoose.connection; 

//Check connection
db.once('open', function() {
    console.log('Connected to mongdb...');
});


//Check for db errore
db.on('error', function(err){
    console.log('ERRORRRRRRRRRRR', err);
});

var app = express();

//Use assets
app.use('/assets', express.static(__dirname + '/assets'));


//Body parser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

//Set the statis path
app.use(express.static(path.join(__dirname, 'public')));

app.post('/bookitem', function (req, res) {
    let booking = new Booking;
    booking.roomID = req.body.id
    booking.name = req.body.name
    booking.phone = req.body.phone;
    booking.email = req.body.email
    booking.motive = req.body.motive
    booking.from = req.body.from
    booking.fromtime = req.body.fromtime
    booking.to = req.body.to
    booking.totime = req.body.totime
    
    booking.save(function(err) {
        if (err) {
            console.log(err);
            res.send('SFAILED');     
            return ;
        } else {
            res.send(req.body.phone);
        }
    });
});


app.listen(3000, function() {
    console.log('server is running on port 3000...')
});

