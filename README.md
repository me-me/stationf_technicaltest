Station-F technical test
=========================


## Description of the project :

Within STATION F, a large number of meeting rooms are planned (48). 
This project will propose a reservation system as a web based application

## Team 
 - abnaceur [Doeremon] : http://naceur-abdeljalil.com

## Project's Goals and objectives

The goal is to code a pile of the reservation system.

Make a small site to search and book one of the meeting rooms, depending on
availability, capacity and equipment. 

We do not need to realize the account management (this is managed by our intranet).

Each reservation must be saved in a .json file

In a second search, if a user is looking for the same date / time, the meeting room
previously booked will not appear in the list of available rooms.

   
    
## Technologies :
    - NodeJs
    - Express framework
    - ReactJs
    - Docker
    - Webpack
    - Bootstrap
    - JQuery
    - Mongodb
 
## Screenshots

## Main page - rooms list fetched from http://online.stationf.co/tests/rooms.json

![Alt text](http://naceur-abdeljalil.com/files/bookinglist.png)

## Room booking page

![Alt text](http://naceur-abdeljalil.com/files/bookingme.png)


## Install the development environment

Get the source:

```bash
git clone https://me-me@bitbucket.org/me-me/stationf_technicaltest.git
```

Edit your `/etc/hosts` file:

```
127.0.0.1   app.bookme.local
```

Build the environment:

```bash
# Use your GITLAB credentials to login in the Docker private registry for the project.
docker-compose up --build
```
Note: the port 80 must not be used by another application (like Apache or Skype).

P.S: The build may take some time don't worry be happy and grab a cup of tea :)


Connect to the container bookme_app and navigate to app/

```bash
# Connect to the container
docker exec -ti bookme_app sh
```

And then 

```bash
# Install all dependencies
cd app/ && npm install
```


### Fix allow control all origin in chrome navigator 
In chrome navigator install the plugin "allow-control-allow-origin"
link :https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

and add the link "http://online.stationf.co/tests/rooms.json" to the allowed origins.

### Help

To generate the bundler in app folder :
```bash.
sudo ./node_modules/.bin/webpack
```

Start webpack in watch mode
```bash
# Install all dependencies.
sudo ./node_modules/.bin/webpack --watch
```

Stop and remove all containers

```bash
docker stop $(docker ps -a -q)
```

Connect to a container via bash (get the container name you want to connect to via command `docker ps`)
```bash
docker exec -ti containername bash
```

Execute a command directly in a container without connecting in bash (get the container name you want to connect to via command `docker ps`)

```bash
docker exec -i containername yourcommand
```

Delete all images 

```bash
docker rmi -f $(docker images -q)
```

Show inages 

```bash
docker images
```