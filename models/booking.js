let mongoose = require('mongoose');

let bookingSchema = mongoose.Schema({
    roomID : {
        type: Number,
    },

    name : {
        type: String,
    },

    email : {
        type: String,
    },

    phone : {
        type: String,
    },

    motive : {
        type: String,
    },

    from: {
        type: String,
    },


    fromtime : {
        type: String,
    },


    to : {
        type: String,
    },


    totime : {
        type: String,
    }
    
});

let Booking = module.exports = mongoose.model('Booking', bookingSchema);