
import React, {Component} from "react";
import Header from './Header';
import Sidebar from './SideBar';
import Footer from './Footer';
import Table from './Table';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const table = "table";
        
        return (
            <div>
                <Header />
                <Sidebar page={table}/>
                <Footer />
            </div>
        );
    }
}

export default Home;