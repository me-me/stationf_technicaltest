import React, {Component} from "react";
import {Redirect, Link, Route, Switch} from "react-router-dom";

class MapTable extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        console.log('equipments', this.props.equipments);
    }

    render() {
        return (
            <div>
                {this.props.equipments.map(equi =>
                    <li>{equi.name}</li>
                )}      
            </div>
        );
        }
    }

export default MapTable;