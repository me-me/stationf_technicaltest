
import React, {Component} from "react";
import Header from './Header';
import Sidebar from './SideBar';
import Footer from './Footer';
import Table from './Table';

class Booking extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const booking = "booking";

        return (
            <div>
                <Header />
                <Sidebar page={booking}/>
                <Footer />
            </div>
        );
    }
}

export default Booking;