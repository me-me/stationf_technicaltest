import React, {Component} from "react";
import {Redirect, Link, Route, Switch} from "react-router-dom";
import MapTable from './MapTable';

class Table extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (

        <section class="content">
        <div class="col-md-9 col-md-offset-2">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">List of rooms</h3>
            </div>
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Capacity</th>
                    <th>Equipment</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    {this.props.rooms.map((room, index) =>
                    <tr> 
                    <td>{room.name}</td>
                    <td>{room.description}</td>
                    <td>{room.capacity}</td>
                    <td>

                    <MapTable equipments={room.equipements}/>
                    
                    </td>
                    <td>
                        <Link to={'/booking?id='+ index}>
                        <button className="btn btn-info">Book</button></Link>
                    </td>
                    </tr>
                )}
                </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
        </section>

        );
        }
    }

export default Table;