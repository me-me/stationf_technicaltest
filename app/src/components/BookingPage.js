
import React, {Component} from "react";
import Header from './Header';
import Sidebar from './SideBar';
import {Redirect, Link, Route, Switch} from "react-router-dom";
import Footer from './Footer';
import Table from './Table';
import MapTable from './MapTable';
import axios from 'axios';

class BookingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rooms : '',
            id : '',
            name: '',
            email: '',
            phone: '',
            motive: '',
            from: '',
            fromtime: '',
            to: '',
            totime: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        axios.get(`http://online.stationf.co/tests/rooms.json`)
        .then(res => {
            const rooms = res.data.rooms;
            let t = window.location.href;
            const id = t.substring(t.length - 1, t.length);
            console.log('id', id);
            console.log('id', this.state.rooms[id]);
            this.setState({
                id: id, 
                rooms : rooms[id]
            });
        });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        const booked = {
            "params": {
                id: this.state.id,
                name: this.state.name,
                email: this.state.email,
                phone: this.state.phone,
                motive: this.state.motive,
                from: this.state.from,
                fromtime: this.state.fromtime,
                to: this.state.to,
                totime: this.state.totime,
            }
        };

        axios.post("/bookitem",{
            id: this.state.id,
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            motive: this.state.motive,
            from: this.state.from,
            fromtime: this.state.fromtime,
            to: this.state.to,
            totime: this.state.totime}
           ).then(res => {
            console.log('response', res);    
        });
    }

    render() {
        console.log('this room ', this.state.rooms);
            
        return (
            <section class="content">
            <div class="col-md-9 col-md-offset-2">
            <div class="col-xs-12">
            <div className="sectionform">
                        <form id="myForm" onSubmit={this.handleSubmit} method="post" enctype="multipart/form-data">
                  
                            <div className="form-group row">
                                <label for="name" className="col-md-2 col-form-label">Name :</label>
                                <div className="col-md-5">
                                    <input type="text" id="name" value={this.state.name} className="form-control" placeholder="Name"
                                           name="name"  onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="email" className="col-md-2 col-form-label">Email :</label>
                                <div className="col-md-5">
                                    <input type="email" id="name" className="form-control" placeholder="Email"
                                           name="email" value={this.state.email} onChange={this.handleChange}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="phone" className="col-md-2 col-form-label">Phone :</label>
                                <div className="col-md-5">
                                    <input type="mobile" id="phone" className="form-control" placeholder="Phone"
                                           name="phone" value={this.state.phone} onChange={this.handleChange}/>
                                </div>
                            </div>

                             <div className="form-group row">
                                <label for="motive" className="col-md-2 col-form-label">Motive :</label>
                                <div className="col-md-5">
                                    <textarea name="motive" 
                                              className="form-control" rows="5" id="motive"
                                              placeholder="Motive" value={this.state.motive}
                                              onChange={this.handleChange}></textarea>
                                </div>
                            </div>

                                <div className="form-group row">
                                <label for="from" className="col-md-2 col-form-label">From :</label>
                                <div className="col-md-5">
                                    <input type="date" id="from" className="form-control" placeholder="Date"
                                           name="from" value={this.state.from} onChange={this.handleChange}/>
                                
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="fromtime" className="col-md-2 col-form-label">Indicate the time :</label>
                                <div className="col-md-5">
                                    <input type="time" id="fromtime" className="form-control" placeholder="Time"
                                           name="fromtime" value={this.state.fromtime} onChange={this.handleChange}/>
                                
                                </div>
                            </div>



                                <div className="form-group row">
                                <label for="to" className="col-md-2 col-form-label">To :</label>
                                <div className="col-md-5">
                                    <input type="date" id="to" className="form-control" placeholder="Date"
                                           name="to" value={this.state.to} onChange={this.handleChange}/>
                                
                                </div>
                            </div>

                            <div className="form-group row">
                                <label for="totime" className="col-md-2 col-form-label">Indicate the time :</label>
                                <div className="col-md-5">
                                    <input type="time" id="totime" className="form-control" placeholder="Time"
                                           name="totime" value={this.state.totime} onChange={this.handleChange}/>
                                
                                </div>
                            </div>


                            <div className="form-group row">
                                <div className="col-md-5 pull-right">
                                    <button type="submit" className="btn btn-primary">Confirme booking</button>
                                    <Link to="/">
                                    <button className="btn btn-default">Cancel</button></Link>
                                </div>
                            </div>

                            <p>* Obligatory input</p><br></br>
                        </form>
                            <div class="panel panel-default">
                            <div class="panel-heading">Booking information</div>
                            <div class="panel-body">
                            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Capacity</th>
                </tr>
                </thead>
                <tbody>
                    <tr> 
                    <td>{this.state.rooms.name}</td>
                    <td>{this.state.rooms.description}</td>
                    <td>{this.state.rooms.capacity}</td>
                    </tr>
                </tbody>
                </table>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        );
    }
}

export default BookingPage;